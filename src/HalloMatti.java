/**
 * 
 */

/**
 * @author matti frind
 */
public class HalloMatti {
	
	protected static void rechne() {
		int a = 7497;
		int b = 937;
		double variable = ((double)a) / b;
		System.out.println("Ergebnis ist " + variable);
		variable = 12 + Integer.valueOf("12");
		System.out.println("Ergebnis ist " + variable);		
	}
	
	protected static void zeichenkette() {
		String antwort = "";
		for (int i = 0; i < 20; i++) {
			antwort = antwort + "a";
		}
		System.out.println(antwort);
		
		String Matti = "";
		for (int i = 0; i < 10; i++) {
		Matti = Matti + "b";	
		}
		System.out.println(Matti);		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		for (int i = 0; i < 2; i++) {
			rechne();
			zeichenkette();
			System.out.println();
		}
		
	}

}
