import java.awt.Color;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class MattiWindow {
	private JTextField textField;
	private JLabel ausgabe;
	private List<JCheckBox> checkBoxes;
	

	protected void zeichne(JPanel aPanel) {
		aPanel.setLayout(new GridLayout(0,2));
		
		JLabel tempLabel = new JLabel();
		tempLabel.setText("Schreib hier deine Gr��e hin");
		aPanel.add(tempLabel);
		
		textField = new JTextField();
		aPanel.add(textField);
		
		JButton tempButton = new JButton();
		tempButton.setText("dr�ck mich");
		tempButton.setBackground(Color.YELLOW);
		tempButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String tempValue = textField.getText();
				tempValue = tempValue+" ";
				String tempDelimiter = "";
				for (int i = 0; i < checkBoxes.size(); i++) {
					JCheckBox tempCheckBox = checkBoxes.get(i);
					if (tempCheckBox.isSelected()) {
						tempValue = tempValue + tempDelimiter + tempCheckBox.getText();
						tempDelimiter = " und ";
					}
				}
				ausgabe.setText(tempValue);
			}
		});
		tempButton.setToolTipText("Dr�ck und schau es dir an.");
		aPanel.add(tempButton);
		
		ausgabe = new JLabel();
		aPanel.add(ausgabe);
		
		JPanel tempSubPanel = new JPanel();
		tempSubPanel.setLayout(new GridLayout(0, 1));
		aPanel.add(tempSubPanel);
		
		checkBoxes = new ArrayList<JCheckBox>();
		JCheckBox tempCheckBox = new JCheckBox();
		tempCheckBox.setText("Emilie");
		tempSubPanel.add(tempCheckBox);
		checkBoxes.add(tempCheckBox);
		
		tempCheckBox = new JCheckBox();
		tempCheckBox.setText("Oma");
		tempSubPanel.add(tempCheckBox);
		checkBoxes.add(tempCheckBox);
		
		tempCheckBox = new JCheckBox();
		tempCheckBox.setText("Opa");
		tempSubPanel.add(tempCheckBox);
		checkBoxes.add(tempCheckBox);
		
		tempCheckBox = new JCheckBox();
		tempCheckBox.setText("Kati");
		tempSubPanel.add(tempCheckBox);
		checkBoxes.add(tempCheckBox);
		
		tempCheckBox = new JCheckBox();
		tempCheckBox.setText("Stephan");
		tempSubPanel.add(tempCheckBox);
		checkBoxes.add(tempCheckBox);
		
		tempCheckBox = new JCheckBox();
		tempCheckBox.setText("Matti");
		tempSubPanel.add(tempCheckBox);
		checkBoxes.add(tempCheckBox);
		
		tempCheckBox = new JCheckBox();
		tempCheckBox.setText("Karin");
		tempSubPanel.add(tempCheckBox);
		checkBoxes.add(tempCheckBox);
		
		tempCheckBox = new JCheckBox();
		tempCheckBox.setText("Erik");
		tempSubPanel.add(tempCheckBox);
		checkBoxes.add(tempCheckBox);
		
		tempCheckBox = new JCheckBox();
		tempCheckBox.setText("Luzie");
		tempSubPanel.add(tempCheckBox);
		checkBoxes.add(tempCheckBox);
		
		tempCheckBox = new JCheckBox();
		tempCheckBox.setText("Conni");
		tempSubPanel.add(tempCheckBox);
		checkBoxes.add(tempCheckBox);
		
		tempCheckBox = new JCheckBox();
		tempCheckBox.setText("Pepi");
		tempSubPanel.add(tempCheckBox);
		checkBoxes.add(tempCheckBox);
		
		
		

	}
	
	public void anzeigen() {
		JFrame tempWindow = new JFrame("Matti");
		tempWindow.setSize(300, 300);
		tempWindow.getContentPane().setLayout(new GridLayout(1, 1));
		JPanel tempPanel = new JPanel();
		tempWindow.getContentPane().add(tempPanel);
		zeichne(tempPanel);
		tempWindow.setVisible(true);
		
		tempWindow.addWindowListener(new WindowListener() {
			
			@Override
			public void windowOpened(WindowEvent e) {				
			}
			
			@Override
			public void windowIconified(WindowEvent e) {
			}
			
			@Override
			public void windowDeiconified(WindowEvent e) {
			}
			
			@Override
			public void windowDeactivated(WindowEvent e) {				
			}
			
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);				
			}
			
			@Override
			public void windowClosed(WindowEvent e) {		
			}
			
			@Override
			public void windowActivated(WindowEvent e) {
			}
		});
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MattiWindow window = new MattiWindow();
		window.anzeigen();
	}

}
